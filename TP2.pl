﻿%Adoua Ange ADOA10119606
%Jefferson Faustin FAUJ08079202
%Omar Bichou BICO27129206
:- use_module(library(lists)).
:- ['donneesTP2.pl'].

/* 0) Le predicat listeFilms(L) lie L à la liste (comportant les identifiants) de tous les films. 
Exemple: 
        ?- listeFilms(ListeDesFilms).
        ListeDesFilms = [sica, nuit, coupe, wind, avengers, iron, sherlock, wind2].
*/
listeFilms(L) :- findall(X, film(X,_,_,_,_,_,_,_,_), L).

/* 
1) 0.25pt. Le predicat listeActeurs(L) unifie L à la liste (comportant les identifiants) de tous les acteurs. 
*/

listeActeurs(L):- findall(X, acteur(_, _, _, _, X), L).


/* 2) 0.25pt. Le predicat experience(IdAct,Annee,Ne) unifie Ne au nombre d'années d'expérience à l'année Annee, de l'acteur dont l"identifiant est IdAct. 
precondition: Annee doit être définie. 
*/ 

experience(X, Y, Z):- acteur(_, _, _, date(V, _, _), X), Z is Y - V.


/* 
3) 0.75pt. Le predicat filtreCritere unifie ActId à l'identifiant du premier acteur qui verifie tous les criteres de Lc. 
precondition: Lc doit etre defini. 
*/									  

filtrereCritere([], _).
filtreCritere([X], Id):-  critere(X, (acteur(_, _, _, _, Id))), !.
filtreCritere([X|XS], Id):- critere(X, (acteur(_, _, _, _, I))), filtreCritere(XS, I), Id = I, !.



/* 
4) 0.75pt. Le predicat totalSalaireMin(LActeur,Total) calcule la somme des salaires minimuns exigés par les acteurs dont la liste (des identifiants) est spécifiée. 
*/


totalSalaireMin([], 0).
totalSalaireMin([X|XS], T):- acteur(_, _, R, _, X), totalSalaireMin(XS, T1), T is R + T1.


/* 
5a) 0.75pt. Le prédicat selectionActeursCriteres(Lcriteres,Lacteurs) unifie Lacteurs à la liste formée des identifiants des acteurs qui satisfont tous les critères de Lcriteres.
precondition: la liste de criteres doit être définie. 
*/

selectionActeursCriteres(Lcriteres, L) :- listeActeurs(L1), findall(X,(member(X, L1),filtreCritere(Lcriteres,X)),L).


/* 
5b) 1pt. Le prédicat selectionActeursCriteresNouvelle(Lcriteres,Lacteurs,LChoisis) unifie LChoisis à la liste formée des identifiants des acteurs sélectionnés parmi les acteurs dans Lacteurs selon 
le principe suivant (jusqu'à concurrence de N acteurs, N correspondant au nombre de critères dans LCriteres: le premier acteur qui satisfait le premier critere de Lcriteres, le premier acteur 
non encore sélectionné et qui satisfait le deuxième critère etc.	
precondition: la liste de criteres (Lcriteres) et la liste des acteurs contenant leurs idenfiants (Lacteurs) doivent être définies. 

*/

selectionActeursCriteresNouvelle([], _, []):- !.
selectionActeursCriteresNouvelle([Critere|Lcriteres], Lacteurs, LChoisis):-  member(A, Lacteurs), filtreCritere([Critere], A), delete(Lacteurs, A, L2), selectionActeursCriteresNouvelle(Lcriteres, L2, L), append([A], L, LChoisis), !.  

/* 
6) 1pt. Le prédicat filmsAdmissibles(ActId,LFilms) unifie LIdFilms à la liste des films (identifiants) satisfaisant les restrictions de l'acteur ActId. 
*/

filmAdmissibles(ActId, LFilms):- acteur(_, _, _, _, ActId), findall(F, (call(ActId, (film(F,_,_,_,_,_,_,_,_)))), L), list_to_set(L, LFilms).


/* 
7a) 1pt. Le prédicat selectionActeursFilm(IdFilm,Lacteurs) unifie Lacteurs à la liste formée des identifiants d'acteurs pour lesquels le film de d'identifiant IdFilm satisfait les restrictions.
préconditions: IdFilm doit être défini 
*/

selectionActeursFilm(IdFilm, Lacteurs):- findall(A, (acteur(_, _, _, _, A), call(A, film(IdFilm, _, _, _, _, _, _, _, _))), L), list_to_set(L, Lacteurs).

/*  
7b) 1pt. Le prédicat selectionNActeursFilm2(IdFilm,Lacteurs) unifie Lacteurs à la liste formée des identifiants des N premiers acteurs pour lesquels le film d'identifiant IdFilm satisfait les restrictions.
         N étant le nombre d'acteurs du film. Si le nombre total des acteurs qualifiés est inférieur au nombre d'acteurs du film, la liste retournée (Lacteurs) devra contenir l'atome pasAssezDacteur.
préconditions: IdFilm doit être défini 
*/
take(N, _, Xs) :- N =< 0, !, N =:= 0, Xs = [].
take(_, [], []):-!.
take(N, [X|Xs], [X|Ys]) :- M is N-1, take(M, Xs, Ys).

selectionNActeursFilm2(IdFilm, Lacteurs):- selectionActeursFilm(IdFilm, L1), film(IdFilm, _, _, _, _, _, _, N, _), length(L1, T), T<N, Lacteurs = 'pasAssezDacteur',!. 
selectionNActeursFilm2(IdFilm, Lacteurs):- selectionActeursFilm(IdFilm, L1), film(IdFilm, _, _, _, _, _, _, N, _), take(N, L1, Lacteurs).


/* 
8) 1pt. Le prédicat acteurJoueDansFilm(Lacteurs, IdFilm) ajoute dans la base de faits tous les acteurs (identifiants) jouant dans le film de titre spécifié (IdFilm) 
*/

acteurJoueDansFim([], _).
acteurJoueDansFilm(Lacteurs, IdFilm):- forall(member(X,Lacteurs), assert(joueDans(X,IdFilm))).


/* 
9a) 1pt. Le prédicat affectationDesRolesSansCriteres(IdFilm) a pour but de distribuer les rôles à une liste d'acteurs pouvant jouer dans le film identifié par IdFilm (puisque
le film satisfait à ses restrictions). Les N premiers acteurs dont les restructions sont respectées par le film (N correspondant au nombre de rôles du film), sont ajoutés dans
dans la base de faits par des prédicats "joueDans".
Ce prédicat modifie le fait film correspondant à IdFilm par destruction et remplacement par un nouveau fait film égal à l'ancien mais dont le budget a été remplacé par la somme des salaires minimums des acteurs choisis et son coût a été diminué de la différence entre le budget initial et le nouveau budget.
Ce prédicat complète la base de faits joueDans(IdActeur, IdFilm) en fonction des N acteurs sélectionnés et dont la somme des salaires minimums est inférieure ou égale au budget (salarial) du film. Le prédicat doit envisager toutes les combinaisons possibles des N acteurs tirés de la base de faits acteur 
Le prédicat échoue et ne modifie rien si une des conditions suivantes est vérifiée (dans l'ordre):
  0) les rôles ont déjà été distribués por ce film
  1) le réalisateur du film est pasDeRealisateur
  2) le producteur du film est pasDeProducteur
  3) s'il n'y a pas assez d'acteurs,
  4) si le budget du film est insuffisant.
précondition: L'identifiant du film doit être défini.
*/
replaceFact(OldFact, NewFact) :- call(OldFact), !, retract(OldFact), assert(NewFact).

%Verification des conditions
affectationDesRolesSansCriteres(IdFilm) :- film(IdFilm,_,_,_,_,_,_,NbA,_), NbA = 0,fail. %les rôles ont déjà été distribués por ce film
affectationDesRolesSansCriteres(IdFilm) :- film(IdFilm,_,_,R,_,_,_,_,_), R = 'pasDeRealisateur',fail. %le réalisateur du film est pasDeRealisateur
affectationDesRolesSansCriteres(IdFilm) :- film(IdFilm,_,_,_,P,_,_,_,_), P = 'pasDeProducteur',fail. %le producteur du film est pasDeProducteur
affectationDesRolesSansCriteres(IdFilm) :-  selectionNActeursFilm2(IdFilm, Lacteurs), Lacteurs = 'pasAssezDacteur',fail. %s'il n'y a pas assez d'acteurs

%Faire une affectation											 
affectationDesRolesSansCriteres(IdFilm) :-  preAffectation(IdFilm,L), length(L,0),
                                             selectionNActeursFilm2(IdFilm, Lacteurs),
                                             film(IdFilm,Titre,Type,R,P,Cout,Duree,NbA,Budget),
                                             totalSalaireMin(Lacteurs,S),
											 Budget >= S,
                                             NouveauCout is Budget - S ,
                                             replaceFact(film(IdFilm,Titre,Type,R,P,Cout,Duree,NbA,Budget),film(IdFilm,Titre,Type,R,P,NouveauCout,Duree,NbA,S)),
											 acteurJoueDansFilm(Lacteurs, IdFilm).

%Ne pas faire une affectation si elle a déjà été faite
preAffectation(IdFilm,L) :- findall((X), joueDans(X,IdFilm), L).

/*
9b) 1pt. Le prédicat affectationDesRolesCriteres(IdFilm,Lcriteres,LChoisis) unifie LChoisis à la liste d'acteurs satisfaisant aux critères de sélection du film, Ce film doit bien entendu satisfaire aux restrictions de 
chacun des acteurs candidat. 
Dans ce prédicat, IdFilm est un identifiant de film et Lcriteres est une liste de critères. 
Pour la satisfaction des critère, on retiendra toujours le premier acteur satisfaisant au 1er critère et on recommensera avec le même principe pour les autres acteurs et les critères restants.
Contrairement au prédicat affectationDesRolesSansCriteres défini à la question 9a, affectationDesRolesCriteres ne modifie pas ba base de faits et se contente de récupérer la liste des acteurs sélectionnés dans Lchoisis.
Le prédicat échoue
  1) si la liste des critère est vide,
  2) si le réalisateur du film est pasDeRealisateur,
  3) si le producteur du film est pasDeProducteur,
  4) s'il n'y a pas assez d'acteurs.
précondition: L'identifiant du film et la liste de critères doivent être définis.
Attention: Il est possible qu'il y ait moins de critère que d'acteurs admissibles. Dans ce cas, la liste des acteurs sélectionnés ne peut dépasser le nombre de critères dans Lcriteres.
           Le nombre maximum d'acteurs choisis est donc égal à la taille de la liste Lcriteres.
*/
affectationDesRolesCriteres(IdFilm,_,_):- film(IdFilm,_,_,R,_,_,_,_,_), R = 'pasDeRealisateur',fail. %le réalisateur du film est pasDeRealisateur
affectationDesRolesCriteres(IdFilm,_,_):- film(IdFilm,_,_,_,P,_,_,_,_), P = 'pasDeProducteur',fail. %le producteur du film est pasDeProducteur
affectationDesRolesCriteres(IdFilm,_,_):- selectionActeursFilm(IdFilm, Lacteurs), film(IdFilm, _, _, _, _, _, _, N, _), length(Lacteurs, T), T<N, Lacteurs = 'pasAssezDacteur',fail. %s'il n'y a pas assez d'acteurs

affectationDesRolesCriteres(IdFilm,Lcriteres,LChoisis):- \+length(Lcriteres,0),selectionActeursFilm(IdFilm, Lacteurs), selectionActeursCriteresNouvelle(Lcriteres,Lacteurs,LChoisis).

/*
10) 2pts. Le prédicat affectationDesRoles(IdFilm, Lcriteres) a pour but de distribuer les rôles à une liste d'acteurs pouvant jouer dans le film et satisfaisant
aux critères de sélection du film en ajoutant les acteurs choisis dans la base de faits "joueDans".
Dans ce prédicat, IdFilm est un identifiant de film et Lcriteres est une liste de critères. 
Pour la satisfaction des critère, on retiendra toujours le premier acteur satisfaisant au 1er critère et on recommensera avec le même principe pour les autres acteurs et les critères restants.
Ce prédicat modifie le fait film correspondant à IdFilm par destruction et remplacement par un nouveau fait film égal à l'ancien mais dont le budget a été remplacé par la somme des salaires minimums des acteurs choisis et son coût a été diminué de la différence entre le budget initial et le nouveau budget.
Ce prédicat complète la base de faits joueDans(IdActeurActeur, IdFilm) en fonction des n acteurs sélectionnés (où n est le nombre de rôles du film) qui satisfont tous les critères de Lcriteres, pour lesquels le film satisfait leur restrictions et dont la somme des salaires minimums est inférieure ou égale au budget du film. 
Le prédicat doit envisager toutes les combinaisons possibles des n acteurs tirés de la base de faits acteur.
Le prédicat échoue et ne modifie rien 
  1) si le réalisateur du film est pasDeRealisateur,
  2) si le producteur du film est pasDeProducteur,
  3) s'il n'y a pas assez d'acteurs,
  4) si le budget du film est insuffisant pour financer le salaire minimum de tous acteurs sélectionnés.
précondition: L'identifiant du film et la liste de critères doivent être définis.
Attention: 
1) Il est possible qu'il y ait moins de critère que d'acteurs admissibles. Dans ce cas, la liste des acteurs sélectionnés doit être complétée (si possible et à concurrence de nombre de rôles) selon le principe du prédicat affectationDesRolesSansCriteres(IdFilm) de la question 9a.
2) Si la liste Lcriteres est vide, c'est aussi le principe de affectationDesRolesSansCriteres(IdFilm) de la question 9a qui s'applique.
*/
affectationDesRoles(IdFilm, _) :- film(IdFilm,_,_,_,_,_,_,NbA,_), NbA = 0,fail. %les rôles ont déjà été distribués por ce film
affectationDesRoles(IdFilm, _) :- film(IdFilm,_,_,R,_,_,_,_,_), R = 'pasDeRealisateur',fail. %le réalisateur du film est pasDeRealisateur
affectationDesRoles(IdFilm, _) :- film(IdFilm,_,_,_,P,_,_,_,_), P = 'pasDeProducteur',fail. %le producteur du film est pasDeProducteur
affectationDesRoles(IdFilm, _) :- selectionActeursFilm(IdFilm, Lacteurs), film(IdFilm, _, _, _, _, _, _, N, _), length(Lacteurs, T), T<N, Lacteurs = 'pasAssezDacteur',!. %s'il n'y a pas assez d'acteurs

%Modifié la base de fait dans le cas où Lcriteres < Lacteurs selectionné
affectationDesRoles(IdFilm, Lcriteres) :- selectionNActeursFilm2(IdFilm, Lacteurs),
                                           length(Lcriteres,Lc),
                                           length(Lacteurs,La),
										   Lc < La,
										   affectationDesRolesSansCriteres(IdFilm).

%Modifié la base de fait dans le cas où Lcriteres n'est pas < à Lacteurs selectionné
affectationDesRoles(IdFilm, Lcriteres) :- selectionNActeursFilm2(IdFilm, Lacteurs),
                                           preAffectation(IdFilm,L), length(L,0),
										   selectionActeursCriteresNouvelle(Lcriteres, Lacteurs, L1),
										   film(IdFilm,Titre,Type,R,P,Cout,Duree,NbA,Budget),
										   totalSalaireMin(L1,S),
										   Budget >= S,
										   NouveauCout is Budget - S ,
										   replaceFact(film(IdFilm,Titre,Type,R,P,Cout,Duree,NbA,Budget),film(IdFilm,Titre,Type,R,P,NouveauCout,Duree,NbA,S)),
										   acteurJoueDansFilm(L1, IdFilm).


										 

/* 11) 1,25 pts. Le prédicat produire(NomMaison,IdFilm) vérifie si la maison peut produire le film identifié. Il vérifie si le budget de la maison 
est supérieur au cout du film, si le réalisateur n'est pas pasDeRealisateur, et si le producteur est pasDeProducteur. Si la production est possible,
 on diminue le budget de la maison par le coût du film et on remplace le fait 'film' par un nouveau film égal à l'ancien sauf que la composante producteur 
 est égale à NomMaison. 
 Précondition: le nom de la maison et l'identifiant du film doivent être connus. Le prédicat doit échoué si la maison ne peut pas produire le film. 
*/
produire(_,IdFilm) :- film(IdFilm,_,_,R,_,_,_,_,_), R = 'pasDeRealisateur',fail.
produire(NomMaison,IdFilm) :- call(maison(NomMaison, X)), call(film(IdFilm,_,_,_,_,Cout,_,_,_)), 
                              X < Cout, fail.

produire(NomMaison,IdFilm) :- maison(NomMaison, X), film(IdFilm,Titre,Type,R,P,Cout,Duree,NbA,Budget), P = 'pasDeProducteur',
                              X > Cout, 
                              NouveauBudgetMaison is X - Cout,
                              replaceFact(maison(NomMaison, _),maison(NomMaison, NouveauBudgetMaison)),
                              replaceFact(film(IdFilm,Titre,Type,R,P,Cout,Duree,NbA,Budget),film(IdFilm,Titre,Type,R,NomMaison,Cout,Duree,NbA,Budget)).
 
							 
/* 12) 0.75pt. Le prédicat plusieursFilms(N,Lacteurs) unifie Acteurs à la liste des acteurs (comportant leurs NOMS), qui jouent dans au moins N films.
N doit être lié à une valeur au moment de la requête de résolution du but 
*/
occurence(0,_,_).
occurence(N,_,[]):- N \= 0, fail.
occurence(N, X, [X|XS]) :- N>0, N1 is N-1, occurence(N1, X, XS).
occurence(N, X, [Y|YS]) :- X\=Y,occurence(N,X,YS).
plusieursFilms(N,Lacteurs) :- findall(X, joueDans(X,Y), L), 
                              findall(X, (member(X,L),occurence(N,X,L)),L2), sort(L2,L3), 
                              findall(Y, (member(X,L3),acteur(Y,_,_,_,X)),Lacteurs).

/* 13) 1.25pt. Les films réalisés et produits doivent maintenant être distribués dans les cinémas. On vous demande définir le prédicat distribuerFilm(IdFilm,PrixEntree) qui envoie le film identifié par IdFilm à tous les cinémas en spécifiant le prix d'entrée suggéré. 
Ce prédicat doit modifier la base de connaissances en ajoutant le triplet  (IdFilm,0,PrixEntree) dans le répertoire de chacun des cinémas déjà existants.
 */
distribuerFilm(IdFilm,PrixEntree) :- findall(W, maison(W,_), ListeMaison), film(IdFilm,_,_,_,P,_,_,_,_),
                                     member(P, ListeMaison),
                                     findall(Y, cinema(Y,_,_),L),
                                     forall(member(X,L),replaceFact(cinema(X, _, []),cinema(X, _, [(IdFilm,0,PrixEntree)]))).			
